<?php

namespace Drupal\Tests\migrate_conditions\Unit\process;

use Drupal\migrate_conditions\Plugin\migrate\process\StopOnCondition;
use Drupal\Tests\migrate\Unit\process\MigrateProcessTestCase;

/**
 * Tests the stop_on_condition process plugin.
 *
 * @group migrate_conditions
 * @coversDefaultClass \Drupal\migrate\Plugin\migrate\process\StopOnCondition
 */
class StopOnConditionTest extends MigrateProcessTestCase {

  /**
   * @covers ::transform
   * @dataProvider providerTestStopOnCondition
   */
  public function testStopOnCondition($evaluate) {
    $condition = $this->createMock('\Drupal\migrate_conditions\ConditionInterface');
    $condition->expects($this->once())
      ->method('evaluate')
      ->will($this->returnValue($evaluate));
    $condition_manager = $this->createMock('\Drupal\Component\Plugin\PluginManagerInterface');
    $condition_manager->expects($this->once())
      ->method('createInstance')
      ->will($this->returnValue($condition));

    $value = 123;
    $configuration = [
      'condition' => 'foo',
    ];
    $plugin = new StopOnCondition($configuration, 'stop_on_condition', [], $condition_manager);
    $pass_through = $plugin->transform($value, $this->migrateExecutable, $this->row, 'destination_property');
    $this->assertSame($value, $pass_through);
    $this->assertSame($evaluate, $plugin->isPipelineStopped());
  }

  /**
   * Data provider for ::testStopOnCondition().
   */
  public static function providerTestStopOnCondition() {
    return [
      'stop' => [
        'evaluate' => TRUE,
      ],
      'go' => [
        'evaluate' => FALSE,
      ],
    ];
  }

}
