<?php

namespace Drupal\Tests\migrate_conditions\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Tests evaluate_condition:foreach process plugin.
 *
 * @group migrate_conditions
 */
class ForeachVariantTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['migrate', 'migrate_conditions'];

  /**
   * Returns test migration definition.
   *
   * @return array
   */
  public function getDefinition() {
    return [
      'source' => [
        'plugin' => 'embedded_data',
        'data_rows' => [
          [
            'id' => 'whatever',
            'array' => [4, 2],
          ],
        ],
        'ids' => [
          'id' => ['type' => 'string'],
        ],
      ],
      'process' => [
        'dest_1' => [
          [
            'plugin' => 'evaluate_condition',
            'source' => 'array',
            'condition' => [
              'plugin' => 'greater_than',
              'value' => 3,
            ],
          ],
        ],
        'dest_2' => [
          [
            'plugin' => 'evaluate_condition:foreach',
            'source' => 'array',
            'condition' => [
              'plugin' => 'greater_than',
              'value' => 3,
            ],
          ],
        ],
      ],
      'destination' => [
        'plugin' => 'config',
        'config_name' => 'migrate_test.settings',
      ],
    ];
  }

  /**
   * Tests the foreach variant.
   */
  public function testForeachVariant() {
    $definition = $this->getDefinition();
    $migration = \Drupal::service('plugin.manager.migration')->createStubMigration($definition);

    $executable = new MigrateExecutable($migration);
    $result = $executable->import();

    // Migration needs to succeed before further assertions are made.
    $this->assertSame(MigrationInterface::RESULT_COMPLETED, $result);

    // Compare with expected data.
    $expected = [
      'dest_1' => TRUE,
      'dest_2' => [TRUE, FALSE],
    ];
    $this->assertSame($expected, \Drupal::config('migrate_test.settings')->get());
  }

}
