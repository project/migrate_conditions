<?php

namespace Drupal\migrate_conditions\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_conditions\Plugin\ProcessPluginWithConditionBase;

/**
 * Stops the pipeline and returns current value if the condition is met.
 *
 * This plugin does not in any way change the input value.
 *
 * Available configuration keys:
 * - method: What to do if the condition is met. Possible values:
 *   - row: Skips the entire row when an empty value is encountered.
 *   - process: Prevents further processing of the input property when the value
 *     is empty.
 * - condition: The condition plugin to evaluate on each element.
 *   Can be either:
 *   - The id of the condition. This is possible if the condition does not
 *     require any configuration, such as the 'empty' condition.
 *   - An array with a 'plugin' key that is the id of the condition.
 *     Any additional properties will be used as configuration when
 *     creating an instance of the condition.
 *
 * Examples:
 *
 * Provide a random value as a fallback if my_source_number is empty.
 *
 * @code
 * process:
 *   field_number:
 *     -
 *       plugin: stop_on_condition
 *       condition: not:empty
 *       source: my_source_number
 *     -
 *       plugin: callback
 *       callable: rand
 *       unpack_source: true
 *       source: []
 * @endcode
 *
 * Most usage of stop_on_condition could be handled with if_condition
 * except in edge cases. Choose based on your personal preferences.
 *
 * @MigrateProcessPlugin(
 *   id = "stop_on_condition",
 *   handle_multiples = TRUE
 * )
 */
class StopOnCondition extends ProcessPluginWithConditionBase {

  /**
   * Stops the pipeline if the condition is met.
   *
   * @param mixed $value
   *   The input value.
   * @param \Drupal\migrate\MigrateExecutableInterface $migrate_executable
   *   The migration in which this process is being executed.
   * @param \Drupal\migrate\Row $row
   *   The row from the source to process.
   * @param string $destination_property
   *   The destination property currently worked on. This is only used together
   *   with the $row above.
   *
   * @return mixed
   *   The input value, $value.
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if ($this->condition->evaluate($value, $row)) {
      $this->stopPipeline();
    }
    return $value;
  }

}
