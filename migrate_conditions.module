<?php

/**
 * @file
 * Hooks n such for Migrate Conditions.
 */

/**
 * Create :foreach process plugin variants that do not handle multiples.
 *
 * For example, if the my_array is an array [4, 2]...
 *
 * @code
 * my_dest:
 *   plugin: evaluate_condition
 *   condition: greater_than(3)
 *   source: my_array
 * @endcode
 *
 * will result in my_dest being TRUE since any array is greater than 3.
 *
 * However...
 *
 * @code
 * my_dest:
 *   plugin: evaluate_condition:foreach
 *   condition: greater_than(3)
 *   source: my_array
 * @endcode
 *
 * will result in my_dest being [TRUE, FALSE] since 4 is greater than 3 while
 * 2 is less than three.
 *
 * Implements hook_migrate_process_info_alter().
 */
function migrate_conditions_migrate_process_info_alter(&$definitions) {
  $iterating_versions = [];
  foreach ($definitions as $definition) {
    if ($definition['provider'] === 'migrate_conditions' && isset($definition['handle_multiples'])) {
      $id = $definition['id'] . ':foreach';
      $iterating_version = $definition;
      $iterating_version['id'] = $id;
      $iterating_version['handle_multiples'] = FALSE;
      $iterating_versions[$id] = $iterating_version;
    }
  }
  $definitions = array_merge($definitions, $iterating_versions);
}
